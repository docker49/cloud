#!/bin/sh

. /common/env.sh

if [ -s /.config/config ] ; then
    set -a

    load_env POSTGRES_USER NEXTCLOUD_DB_USER
    load_env POSTGRES_PASSWORD NEXTCLOUD_DB_PASS
    load_env NEXTCLOUD_USER
    load_env NEXTCLOUD_PASS
    load_env HOST NEXTCLOUD_HOST
    load_env TRANS_HOST

    eval "echo \"$(cat /res/reverse_vhost.conf)\"" |sed 's/%/$/' > /sites/${HOST}.conf

    chown -R www-data:www-data /var/www/html

    # wait for the db
    while ! (php -r 'exit(intval(!fsockopen(getenv("POSTGRES_HOST"), 5432)));') >/dev/null 2>&1; do sleep 1; done;
    sleep 5

    su -p www-data -s /bin/sh -c "/bin/sh /res/post.sh"

    unset POSTGRES_USER
    unset POSTGRES_PASSWORD
    unset NEXTCLOUD_USER
    unset NEXTCLOUD_PASS

    exit 0
fi

exec busybox crond -f -l 0 -L /dev/stdout&

php-fpm
