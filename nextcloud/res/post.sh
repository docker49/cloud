#!/bin/sh

cd /var/www/html

echo "Installing"
php occ maintenance:install -n --admin-user "${NEXTCLOUD_USER}" --admin-pass "${NEXTCLOUD_PASS}" \
    --database pgsql --database-name "${POSTGRES_DB}" --database-user "${POSTGRES_USER}" \
    --database-pass "${POSTGRES_PASSWORD}" --database-host "${POSTGRES_HOST}"

php occ config:system:set overwriteprotocol --value="https"
php occ config:system:set trusted_domains 1 --value="web"
php occ config:system:set overwritehost --value=${HOST}

# Email configuration
php occ user:setting "${NEXTCLOUD_USER}" settings email "${NEXTCLOUD_USER_EMAIL}"

php occ config:system:set mail_from_address "notification@${HOST}"
php occ config:system:set mail_smtpmode "smtp"
php occ config:system:set mail_sendmailmode "smtp"
php occ config:system:set mail_smtphost "smtp"
php occ config:system:set mail_smtpport "25"
#php occ config:system:set mail_domain "25"

php occ background:cron

echo "Installed"

echo "Appplication install"
for app in "news" "breezedark" "calendar" "notes" "external" "twofactor_totp"; do
	php occ app:install ${app};
done

 ./occ config:app:set external sites --value='{"1":{"id":1,"name":"trans","url":"https://'${TRANS_HOST}'","lang":"","type":"link","device":"","icon":"external.svg","groups":[
],"redirect":true}}'
