#!/bin/sh

. /common/env.sh


if [ ! -s /.config/config ]; then
	if [ "${SELF_SIGNED}" != "true" ] ; then
		while true; do
			sleep 30d
			/certbot_renew.sh
		done
	fi

	exit 0
fi

load_env SELF_SIGNED
load_env CN
load_env ORG
load_env COUNTRY
load_env STATE
load_env LOCALITY
load_env CRT_BASE_FILE
load_env CERTBOT_EMAIL
load_env CERTBOT_STAGING
load_env RPROXY_HOST
load_env CERTBOT_OVD_ENDPOINT
load_env CERTBOT_OVD_APP_KEY
load_env CERTBOT_OVD_APP_SECRET
load_env CERTBOT_OVD_CONSUMER_KEY


if [ "${SELF_SIGNED}" == "true" ] ; then
	if [ ! -f /crt/${CRT_BASE_FILE}.crt ]; then
		mkdir -p /crt/private
		openssl req -x509 -nodes -days 365 \
			-subj "/C=${COUNTRY}/ST=${STATE}/L=${LOCALITY}/O=${ORG}/CN=${CN}" \
			-addext "subjectAltName=DNS:${RPROXY_HOST},DNS:*.${RPROXY_HOST}" \
			-newkey rsa:2048 -keyout /crt/private/${CRT_BASE_FILE}.key \
			-out /crt/${CRT_BASE_FILE}.crt;
	fi
	exit 0
fi

STAGING_ARGS=""
if [ "${CERTBOT_STAGING}"  == "true" ]; then
	STAGING_ARGS="--staging"
fi

cat << EOF > /crt/ovh.ini
# OVH API credentials used by Certbot
dns_ovh_endpoint = ${CERTBOT_OVD_ENDPOINT}
dns_ovh_application_key = ${CERTBOT_OVD_APP_KEY}
dns_ovh_application_secret = ${CERTBOT_OVD_APP_SECRET}
dns_ovh_consumer_key = ${CERTBOT_OVD_CONSUMER_KEY}
EOF

chmod 600 /crt/ovh.ini

cat << EOF > /certbot_renew.sh
#!/bin/sh

	if [ ! -f /crt/certbot ]; then
		certbot certonly -n --dns-ovh \
			--dns-ovh-credentials  /crt/ovh.ini \
			--agree-tos \
			${STAGING_ARGS} \
			-m ${CERTBOT_EMAIL} \
			-d ${RPROXY_HOST},*.${RPROXY_HOST} \
			--rsa-key-size 4096 \
			--renew-hook 'sh -c "touch /crt/certbot"'
	else
		certbot certonly -n --dns-ovh \
			--dns-ovh-credentials  /crt/ovh.ini \
			--agree-tos \
			${STAGING_ARGS} \
			-m ${CERTBOT_EMAIL} \
			-d ${RPROXY_HOST},*.${RPROXY_HOST} \
			--rsa-key-size 4096 \
			--force-renewal
	fi

	mkdir -p /crt/private
	cp /etc/letsencrypt/live/${RPROXY_HOST}/privkey.pem /crt/private/${CRT_BASE_FILE}.key
	cp /etc/letsencrypt/live/${RPROXY_HOST}/fullchain.pem /crt/${CRT_BASE_FILE}.crt
EOF

chmod +x /certbot_renew.sh

if [ ! -f /crt/certbot ]; then
	/certbot_renew.sh
fi
