#!/bin/sh

. /common/env.sh

wait_change () {
     while true; do
          inotifywait \
               -r -q \
               -e modify -e close_write \
               -e create -e move -e move_self \
               -e delete -e delete_self \
          /sites /crt

          echo "Restart nginx"
          nginx -s reload
	done
}

if [ -s /.config/config ] ; then
     if [ ! -f /usr/bin/inotifywait ]; then
          apk add inotify-tools
     fi

     load_env HOST RPROXY_HOST

     # Copy needed file
     cp -fr /res/ssl /etc/nginx/
     cp -f /res/nginx.conf /etc/nginx/

     until [ -f /crt/site.crt ]
     do
          sleep 1
     done
     echo "Certificate ready"

     eval "echo \"$(cat /res/vhost.conf)\"" |sed 's/%/$/' > /sites/${HOST}.conf
     exit 0
fi

wait_change&

nginx -g "daemon off;"