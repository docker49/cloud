#!/bin/bash

. /common/env.sh

if [ -s /.config/config ] ; then
    set -a

    load_env HOST BITWARDEN_HOST
    load_env BITWARDEN_ADMIN_TOKEN
    load_env BITWARDEN_DB_USER
    load_env BITWARDEN_DB_PASS
    load_env BITWARDEN_DB
    load_env BITWARDEN_SMTP_FROM

    # create bitwarden configuration file from template
    eval "echo \"$(cat /res/config.json.tpl | sed "s/\"/'/g")\"" | sed "s/'/\"/g" > /data/config.json
    eval "echo \"$(cat /res/reverse_vhost.conf)\"" |sed 's/%/$/' > /sites/${HOST}.conf

    exit 0
fi

# wait for the db
while ! (echo > /dev/tcp/bit_db/5432) >/dev/null 2>&1; do sleep 1; done;
sleep 5

/bitwarden_rs
