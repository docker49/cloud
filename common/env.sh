#!/bin/sh

load_env() {
    if [ -z $2 ]; then
        export $(cat /.config/config | grep $1 | sed -e /^#/d | xargs)
    else
        DATA=$(cat /.config/config | grep $2 |cut -d= -f2 | sed -e /^#/d | xargs) > "/$1"
        if [ -n ${DATA} ]; then
            export $1=$DATA
        fi
    fi
}
