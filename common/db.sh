#!/bin/sh

. /common/env.sh

if [ -z $1 ]; then
    echo "Usage $0 db_preffix"
    exit 0
fi

# load env from file 
if [ -s /.config/config ] && [ ! -s "$PGDATA/PG_VERSION" ]; then
    DB_USER_VAR="${1}_DB_USER"
    DB_PASS_VAR="${1}_DB_PASS"

    set -a
    load_env "POSTGRES_USER" $DB_USER_VAR
    load_env "POSTGRES_PASSWORD" $DB_PASS_VAR
fi

# Fix an issue in docker-entrypoint.sh
if ! grep -q "unset PGPASSWORD POSTGRES_USER POSTGRES_PASSWORD"; then
    sed -i "s/unset PGPASSWORD/unset PGPASSWORD POSTGRES_USER POSTGRES_PASSWORD/g" /usr/local/bin/docker-entrypoint.sh

fi

docker-entrypoint.sh postgres
