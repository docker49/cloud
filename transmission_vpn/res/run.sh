#!/bin/sh

. /common/env.sh

if [ -s /.config/config ] ; then
    load_env HOST TRANS_HOST
    load_env HT_USER TRANS_HT_USER
    load_env HT_PASS TRANS_HT_PASS
    load_env OPENVPN_USERNAME TRANS_VPN_USER
    load_env OPENVPN_PASSWORD TRANS_VPN_PASS

    eval "echo \"$(cat /res/reverse_vhost.conf)\"" |sed 's/%/$/' > /sites/${HOST}.conf

    if [ -n "${HT_USER}" ] && [ -n "${HT_PASS}" ]; then
        echo "${HT_USER}:$(openssl passwd --apr1 ${HT_PASS})" > /sites/.htpasswd
        sed -i 's/#auth/auth/g' /sites/${HOST}.conf
        unset HT_USER
        unset HT_PASS
    fi

    touch /config/use_vpn
    if [ "${OPENVPN_PROVIDER}" = "**None**" ]; then
        rm -f /config/use_vpn
    fi

    echo $OPENVPN_USERNAME > /config/openvpn-credentials.txt
    echo $OPENVPN_PASSWORD >> /config/openvpn-credentials.txt
    chmod 600 /config/openvpn-credentials.txt
    export OPENVPN_USERNAME="**None**"
    export OPENVPN_PASSWORD="**None**"

    exit 0
fi

if [ -f /config/use_vpn ]; then
    dumb-init /etc/openvpn/start.sh
else
    # Hack to not use th vpn
    dockerize -template /etc/transmission/environment-variables.tmpl:/etc/transmission/environment-variables.sh
    /etc/openvpn/tunnelUp.sh x x x 0.0.0.0
    sleep infinity
fi
