# docker multi docker-compose deployment

The goal of this project is to deploy somes services behind a reverse proxy
with certbot configured using multi domain approach.

The goal is to deploy:

* a nextcloud integrated with a smtp and configured with some applications
* a transmission compatible with/without vpn
* a bitwarden integrated with the smtp
* a smtp
* a nginx proxy configured using letsencrypt(ovh) or with a self signed certificate.
  ovh api information can by retrived from <https://eu.api.ovh.com/createToken>.

## Environment install
Docker containers are deployed using the command:

```bash
./env start [-f configuration_file]
```

The configuration_file is optional. By default, the script uses config.default.

The goal of this phase is to be able to remove the configuration file after the install.
When it is finished, you can start start the environment using the command:

```bash
./env start
```

Logs can be verified using the command:

```bash
./env logs -f --tail=10
```

## Remarks

* To customize basic thing (password, hostname), you can copy config.default and replace needed password.
  By default transmission does not use openvpn.

* The DNS must contain all hosts declared in the configuration file.
  An alternative is too add a wildcard record.

## Logs consideration
By default, docker does not manage rotation of logs generated by containers.
In order to have log rotate and to prevent disk usage you can:

* Add the following line in the file */etc/docker/daemon.json*:

  ```json
  {
  "log-driver": "json-file",
  "log-opts": {
      "max-size": "10m",
      "max-file": "3"
      }
  }
  ```

* Restart docker service
  ```bash
  service docker restart
  ```


